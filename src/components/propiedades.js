import React from 'react';
import Iconos from './iconos';
import styled from '@emotion/styled';
import Image from 'gatsby-image';
// import { Link } from 'gatsby';
// const urlSlug = require('url-slug');
import Layout from './layout';
import { graphql } from 'gatsby';
import { css } from '@emotion/core';



const Contenido = styled.div`

    max-width: 1200px;
    margin: 0 auto;
    width: 95%;

    @media(min-width:768px){
        display:grid;
        grid-template-columns:2fr 1fr;
        column-gap:5rem;
    }
`;

const Sidebar = styled.aside`

    .precio{
        font-size: 2rem;
        color: #75AB00;
    }

    .agente{
        margin-top:4rem;
        border-radius:2rem;
        background-color: #75AB00;
        padding:3rem;
        color:#FFF;
    }

    p{
        font-weight:900;
    }

`;

export const query = graphql`

query($id: String!){
    allStrapiPropiedades(filter: {id: {eq:$id}}){
        nodes{
            Nombre
            Descripcion
            Precio
            Habitaciones
            Wc
            Estacionamiento
            agentes{
                Nombre
                Telefono
                Email
            }
            Imagen {
                sharp:childImageSharp{
                    fluid(maxWidth:1200){
                        ...GatsbyImageSharpFluid_withWebp
                    }
                }   
            }
        }
    }
}

`;

const Propiedades = ({ data: { allStrapiPropiedades: { nodes } } }) => {

    const { Nombre, Descripcion, Precio, Wc, Habitaciones, Estacionamiento, agentes, Imagen } = nodes[0];

    return (
        <Layout>
            <h1 css={css`
                margin-top: 2rem;
                margin-bottom: 2rem;

            `}>{Nombre}</h1>
            <Contenido>
                <main>
                    <Image fluid={Imagen.sharp.fluid} />
                </main>

                <Sidebar>
                    <p className="precio">${Precio}</p>
                    <Iconos
                        wc={Wc}
                        estacionamiento={Estacionamiento}
                        habitaciones={Habitaciones}
                    />

                    <div className="agente">
                        <h2>Vendedor:</h2>
                        <p>{agentes.Nombre}</p>
                        <p>Tel: {agentes.Telefono}</p>
                        <p>Email: {agentes.Email}</p>
                    </div>
                </Sidebar>
                <p>{Descripcion}</p>
            </Contenido>
        </Layout>
    );
};

export default Propiedades;