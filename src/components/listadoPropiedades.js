import React, { useState, useEffect } from 'react';
import { css } from '@emotion/core';
import UsePropiedades from '../hooks/usePropiedades';
import PropiedadesPreview from '../components/propiedadPreview';
import ListadoPropiedadesCSS from '../css/listadoPropiedades.module.css';
import useFiltro from '../hooks/useFiltro';

const ListadoPropiedades = () => {

    const resultado = UsePropiedades();
    const [propiedades] = useState(resultado);
    const [filtradas,guardarFiltradas] = useState([]);

    const { categoria,FiltroUI } = useFiltro();

    useEffect(() => {

        if(categoria){
            const filtro = propiedades.filter(propiedad=>propiedad.categoria.nombre === categoria);
            guardarFiltradas(filtro);
        }
        else
            guardarFiltradas(propiedades);
    }, [categoria,propiedades]);

    return (
        <>
            <h2 css={css`
            margin-top: 5rem;
            font-weight:400;
        `}>
                Nuestras propiedades
        </h2>

        {FiltroUI()}

            <ul className={ListadoPropiedadesCSS.propiedades}>
                {filtradas.map(propiedad => (
                    <PropiedadesPreview 
                    key={propiedad.id}
                    propiedad={propiedad}
                    />
                ))}
            </ul>
        </>
    );
};

export default ListadoPropiedades;