import {graphql,useStaticQuery} from 'gatsby';

const UsePropiedades = () => {

const datos = useStaticQuery(graphql`
    query{
        allStrapiPropiedades {
          nodes {
            id
            Descripcion
            Estacionamiento
            Habitaciones
            Nombre
            Precio
            Wc
            categoria{
              nombre
            }
            agentes {
              Nombre
              Email
              Telefono
            }
            Imagen {
                sharp:childImageSharp{
                    fluid(maxWidth:600, maxHeight:400){
                        ...GatsbyImageSharpFluid_withWebp
                    }
                }   
            }
          }
        }
      }
        `); 
console.log(datos)
       return datos.allStrapiPropiedades.nodes.map(propiedad=>({
           nombre:propiedad.Nombre,
           descripcion:propiedad.Descripcion,
           imagen:propiedad.Imagen,
           id:propiedad.id,
            wc:propiedad.Wc,
            estacionamiento:propiedad.Estacionamiento,
            habitaciones:propiedad.Habitaciones,
            agentes:propiedad.agentes,
            precio:propiedad.Precio,
            categoria:propiedad.categoria
       }))
};

export default UsePropiedades;