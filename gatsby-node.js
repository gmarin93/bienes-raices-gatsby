const urlSlug = require('url-slug');

exports.createPages = async ({actions,graphql,reporter})=>{

    const resultado = await graphql(`
    
    query {

        allStrapiPaginas{
            nodes{
                nombre
                id
            }
        }
        allStrapiPropiedades {
          nodes {
            id
            Nombre
          }
        }
      }
    `);

    // console.log(JSON.stringify(resultado.data.allStrapiPropiedades));

    //Sihay resultados
    if(resultado.errors)
      reporter.panic('No hubo resultados', resultado.errors);

    const propiedades = resultado.data.allStrapiPropiedades.nodes;
    const paginas = resultado.data.allStrapiPaginas.nodes;


    //templates
    propiedades.forEach(propiedad => {
        actions.createPage({
                path:urlSlug(propiedad.Nombre),
                component: require.resolve('./src/components/propiedades.js'),
                context:{   
                    id:propiedad.id
                }
            })
    });

    //templates para paginas
    paginas.forEach(pagina => {
        actions.createPage({
                path:urlSlug(pagina.nombre),
                component: require.resolve('./src/components/paginas.js'),
                context:{   
                    id:pagina.id
                }
            })
    });
}